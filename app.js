const yargs = require('yargs');
const axios = require('axios');

// const geocode = require('./geocode/geocode');
// const weather = require('./weather/weather');

const argv = yargs
	.options({
		a: {
			demand: true,
			alias: 'address',
			describe: 'Address to fetch weather for',
			string : true
		}
	})
	.help()
	.alias('help', 'h')
	.argv;

// WITH AXIOS ********************
var encodedAddress = encodeURIComponent(argv.address);
var geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

axios.get(geocodeUrl)
	.then((response) => {
		if (response.data.status === 'ZERO_RESULTS') {
			throw new Error('Unable to find that address');
		}
		var lat = response.data.results[0].geometry.location.lat;
		var lng = response.data.results[0].geometry.location.lng;
		var weatherUrl = `https://api.darksky.net/forecast/6432a1d886d22b8054e192b6bd3a1693/${lat},${lng}`;
		console.log(response.data.results[0].formatted_address);
		return axios.get(weatherUrl);
	})
	.then((response) => {
		var temperature = response.data.currently.temperature;
		var apparentTemperature = response.data.currently.apparentTemperature;
		console.log(`It's currently ${temperature}. It feels like ${apparentTemperature}.`);
	})
	.catch((err) => {
		console.log(err);
	})

// WITH PROMISES ********************
// geocode.geocodeAddress(argv.address).then((res) => {
// 	console.log(res.address);
// 	weather.getWeather(res.latitude, res.longitude).then((res) => {
// 		console.log(`It's currently ${res.temperature}. It feels like ${res.apparentTemperature}.`);
// 	}, (errorMessage) => {
// 		console.log(errorMessage);
// 	});
// }, (errorMessage) => {
// 	console.log(errorMessage);
// });

// WITH CALLBACKS ********************
// geocode.geocodeAddress(argv.address, (errorMessage, results) => {
// 	if (errorMessage) {
// 		console.log(errorMessage);
// 	} else {
// 		console.log(results.address);
// 		weather.getWeather(results.latitude, results.longitude, (errorMessage, results) => {
// 			if (errorMessage) {
// 				console.log(errorMessage);
// 			} else {
// 				console.log(`It's currently ${results.temperature}. It feels like ${results.apparentTemperature}.`);
// 			}
// 		});
// 	}
// });