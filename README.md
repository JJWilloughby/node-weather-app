# weather-app
Nodejs async weather app

## Installation
- Clone repo
- Navigate to root of project and `npm install`

## Usage
- Run app with `node app.js -a '<address>'`

## Purpose
This project helped me get more comfortable working with node, accessing the file system, and using callbacks (particularly when receiving information from api calls)
