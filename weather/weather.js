const request = require('request');

module.exports = {

	// WITH PROMISES
	getWeather: (lat, lng) => {
		return new Promise((resolve, reject) => {
			request({
				url: `https://api.darksky.net/forecast/6432a1d886d22b8054e192b6bd3a1693/${lat},${lng}`,
				json: true
			}, (error, response, body) => {
				if (!error && response.statusCode === 200) {
					resolve({
						temperature: body.currently.temperature,
						apparentTemperature: body.currently.apparentTemperature
					});
				} else {
					reject('Unable to fetch weather');
				}
			});
		});
	}

	// WITH CALLBACKS
	// getWeather: (lat, lng, callback) => {
	// 	request({
	// 		url: `https://api.darksky.net/forecast/6432a1d886d22b8054e192b6bd3a1693/${lat},${lng}`,
	// 		json: true
	// 	}, (error, response, body) => {
	// 		if (!error && response.statusCode === 200) {
	// 			callback(undefined, {
	// 				temperature: body.currently.temperature,
	// 				apparentTemperature: body.currently.apparentTemperature
	// 			});
	// 		} else {
	// 			callback('Unable to fetch weather');
	// 		}
	// 	});
	// }
}